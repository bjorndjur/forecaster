import requests

def getForecast(lat,lon, key):
    url = "https://api.openweathermap.org/data/2.5/onecall?lat={lat}&lon={lon}&units=metric&exclude=alerts&appid={key}".format(lat=lat, lon=lon, key=key)
    forecast = requests.get(url).json()
    return forecast

