import location
import sys
import forecast
from datetime import datetime
import time
import pandas as pd
from pathlib import Path
import json
from os.path import expanduser

def getKey(apiName):
    home = expanduser("~")
    path = home + '/apikey.json'
    with open(path, "r") as file:
        key = file.read()
    fileJson = json.loads(key)
    return fileJson['keys'][0][apiName]

key = getKey('openweathermap')

coordinates = location.getCoordinates(sys.argv[1])

forecastLength = int(sys.argv[2]) + 2 if len(sys.argv) >= 3 else 48

df = pd.DataFrame(columns=['Time', 'Description','Temperature (C)', 'Wind (m/s)', 'Probability'])


data = forecast.getForecast(coordinates.latitude,coordinates.longitude, key)

#print(data['hourly'][0]['pop'])

for i in range(int(forecastLength)):
    currentTime = int(time.time())
    ts = data['hourly'][i]['dt']
    # print(datetime.utcfromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S'))
    if (currentTime + 3600 < ts):
        weatherTime = datetime.utcfromtimestamp(ts).strftime('%Y-%m-%d %H:%M')
        description = data['hourly'][i]['weather'][0]['description']
        temperature = int(data['hourly'][i]['temp'])
        probability = data['hourly'][i]['pop']
        wind = round(data['hourly'][i]['wind_speed'],1)
        df = df.append({'Time': weatherTime, 'Description': description, 'Temperature (C)': temperature, 'Wind (m/s)': wind, 'Probability': probability}, ignore_index=True)


print(df.to_string(index=False))
