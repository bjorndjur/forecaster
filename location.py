from geopy.geocoders import Nominatim

def getCoordinates(location):
    geolocator = Nominatim(user_agent='forecaster')
    location = geolocator.geocode(location)
    return location

